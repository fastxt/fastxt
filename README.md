# Fastxt
[fastxt.app](https://fastxt.app)

Fastxt, as a sister project to [Local Native](https://localnative.app), with very similar design and toolchain, is a decentralized cross-platform application to save and sync your txt in local SQLite database without going through any centralized service.

# Videos
[Local Native YouTube Channel](https://www.youtube.com/channel/UCO3qFIyK0eSmqvMknsslWRw)

## Articles
[Updates](https://fastxt.app/blog)


# Sub-directories

fastxt-android: Android App

fastxt-ios: iOS and iPadOS App

fastxt-mac: macOS App

fastxt-rs: rust code, as shared core library used by other presentation layers

fastxt-win: Windows App


# Developer Setup
[here](https://fastxt.app/developer-setup.html)

# License
[AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)


## Screenshot

### Desktop Application
![Fastxt desktop application](https://fastxt.app/img/fastxt-sync-server-qr-mac.png)

## Support
<a href="https://opencollective.com/fastxt/donate" target="_blank">
  <img src="https://opencollective.com/localnative/donate/button@2x.png?color=blue" width=300 />
</a>
